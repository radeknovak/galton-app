- [x] convert to typescript
- [x] homepage is the Record/Edit page
- [x] edit modal to edit page
- [x] tap on row to edit
- [x] rm delete button from row
- [x] rm edit button from row
- [x] delete from the edit page
- [x] fix duplicate button


- [x] fix window resizing when focusing input on mobile
- [x] rm antd

- [ ] ~~introduce taxonomy~~

- [ ] Add record immediately creates new record and opens editing page
- [ ] duplicate button has "copy" icon
