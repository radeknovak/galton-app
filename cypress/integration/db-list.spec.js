import * as sel from '../helpers/selectors';
import { addRecord } from '../helpers/record';

xdescribe('Record groups', function () {
  it('should create a new group', () => {
    cy.visit('/');
    addRecord('abc-firstdb');
    addRecord('bcd-firstdb');

    cy.visit('/options');

    const DB_TEST_STRING = 'ABC-db';

    cy.get(sel.DbAddInput).type(DB_TEST_STRING);
    cy.get(sel.DbAddSubmit).click();

    cy.get(sel.DbList).contains(DB_TEST_STRING).should('exist');

    cy.visit('/');
    cy.get(sel.DbList).contains('firstdb').should('not.exist');
  });
});
