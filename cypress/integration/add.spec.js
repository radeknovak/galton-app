import { render, screen } from '@testing-library/cypress';
import * as sel from '../helpers/selectors';
import * as helpers from '../helpers/record';

describe('Add-Record-Popup', function () {
  beforeEach(() => {
    indexedDB.deleteDatabase('main');
  });

  it('should create record', () => {
    cy.visit('/');

    const TEST_STRING = 'ABC xyz';

    cy.findByLabelText('event / symptom').type(TEST_STRING);
    cy.get('form').submit();

    cy.findByLabelText('event / symptom').should('have.value', '');
  });

  it('should keep record', () => {
    cy.visit('/');

    const TEST_STRING = 'ABC xyz';

    cy.findByLabelText('event / symptom').type(TEST_STRING);
    cy.get('form').submit();

    cy.findByLabelText('event / symptom').should('have.value', '');

    cy.findByRole('menuitem', { name: 'list' }).click();

    cy.get('[data-cy="record-list"]')
      .findByText(TEST_STRING)
      .should('be.visible');
  });
});

describe('Edit-Record', function () {
  beforeEach(() => {
    indexedDB.deleteDatabase('main');

    cy.visit('/');

    helpers.addRecord('A');
    helpers.addRecord('B');
  });

  it('should edit record', () => {
    cy.visit('/list');
    // verify test data
    cy.get('[data-cy="record-list"]').findByText('A').should('be.visible');
    cy.get('[data-cy="record-list"]').findAllByText('B').should('be.visible');

    // act
    cy.get('[data-cy="record-list"]').findByText('A').click();
    cy.findByLabelText('event / symptom').should('have.value', 'A');
    cy.findByLabelText('event / symptom').type('BCDEF');

    cy.get('form').submit();

    // assert
    cy.get('[data-cy="record-list"]').findByText('A').should('not.be.visible');
    cy.get('[data-cy="record-list"]').findByText('B').should('be.visible');
    cy.get('[data-cy="record-list"]').findByText('ABCDEF').should('be.visible');

    cy.findByRole('button', { name: /^A$/ }).should('not.exist');
    cy.findByRole('button', { name: /^B$/ }).should('exist');
    cy.findByRole('button', { name: /^ABCDEF$/ }).should('exist');
  });
});

describe('Clone-Record', function () {
  beforeEach(() => {
    indexedDB.deleteDatabase('main');
  });

  it('should clone record', () => {
    cy.visit('/');
    helpers.addRecord('Test record');

    cy.visit('/list');
    // verify test data

    // act
    cy.findByLabelText('clone').click();
    cy.findByLabelText('event / symptom').should('have.value', 'Test record');
  });
});
