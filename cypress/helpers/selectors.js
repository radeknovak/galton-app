export const AddRecordPopup = '[data-cy="add-record-portal"]';
export const AddRecordButton = '#t-add-record';
export const RecordList = '[data-cy="record-list"]';

export const ItemClone = '[data-cy="item-action-clone"]'
export const ItemEdit = '[data-cy="item-action-edit"]'
export const ItemDelete = '[data-cy="item-action-delete"]'

export const DbAddInput = '[data-cy="db-add-input"]'
export const DbAddSubmit = '[data-cy="db-add-submit"]'
export const DbList = '[data-cy="db-list"]'