import * as sel from './selectors';

// const addRecord = testString => {
//   cy.get(sel.AddRecordButton).click();

//   cy.get('input[name="name"]').type(testString);
//   cy.get(sel.AddRecordPopup + " " + "form").submit();
//   cy.get(sel.RecordList)
//     .contains(testString)
//     .should("exist");

//   // close after submit
//   cy.get(sel.AddRecordPopup).should('not.be.visible');
// }

const addRecord = testString => {
  cy.findByLabelText('event / symptom').type(testString);
  cy.get('form').submit();
};

export { addRecord };
