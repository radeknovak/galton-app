type ValueType = 'MultiSelect' | 'SingleSelect' | 'TextInput' | 'NumberInput';

type BaseValue = {
  label: string;
};

interface MultiSelect extends BaseValue {
  type: 'MultiSelect';
  options: string[];
  custom: boolean;
  value: string;
}

interface SingleSelect extends BaseValue {
  type: 'SingleSelect';
  options: string[];
  custom: boolean;
  value: string;
}

interface TextInput extends BaseValue {
  type: 'TextInput';
  value: string;
}

interface NumberInput extends BaseValue {
  type: 'NumberInput';
  min?: number;
  max?: number;
  step?: number;
  value?: number;
  unit?: string;
}

type Value = MultiSelect | SingleSelect | TextInput | NumberInput;

type Taxon = {
  // id: string;
  parent?: Taxon;
  name: string;
  values?: Value[];
  children?: Taxon[];
};

type Taxonomy = Taxon[];
