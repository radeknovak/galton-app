import React from 'react';
import Portal from './Portal';
import styled from 'styled-components';
import CloseButton from '../Buttons/CloseButton';

const stopPropagation = (e: { stopPropagation: () => void }) => {
  e.stopPropagation();
};
type PropsWrap = {
  isOpen: boolean;
};
const Inner = styled.div<PropsWrap>`
  display: ${props => (props.isOpen ? 'block' : 'none')};
  margin: 10px;
  background: #fff;
`;
const Outer = styled.div<PropsWrap>`
  display: ${props => (props.isOpen ? 'block' : 'none')};
  background: rgba(0, 0, 0, 0.4);
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: 1000;
`;

type Props = {
  isOpen: boolean;
  onClose: () => any;
};

export default class Popup extends React.Component<Props> {
  render() {
    const { onClose, isOpen, children } = this.props;
    return (
      <Portal>
        <Outer onClick={onClose} isOpen={isOpen}>
          <Inner
            data-cy="add-record-portal"
            onClick={stopPropagation}
            isOpen={isOpen}
          >
            <CloseButton onClick={onClose} />
            {children}
          </Inner>
        </Outer>
      </Portal>
    );
  }
}
