import React from 'react';
import { taxonomy } from '../../services/taxonomy';
import './style.css';

type Props = {
  taxonomy: Taxonomy;
  selection: string[];
  level?: number;
  onSelection: (current: Taxon) => void;
};

const TaxonomyRenderer = (props: Props) => {
  const { level = 0 } = props;
  const [selected, ...nextSelection] = props.selection;
  const currentRow: React.ReactNode[] = [];
  const nextRow: typeof taxonomy = [];

  for (let currenttaxon of props.taxonomy) {
    const classNames = ['taxon-btn'];
    if (selected !== undefined) {
      classNames.push(
        selected === currenttaxon.name
          ? 'taxon-btn-selected'
          : 'taxon-btn-deselected'
      );
    }
    currentRow.push(
      <button
        key={level + currenttaxon.name}
        type="button"
        onClick={() => props.onSelection(currenttaxon)}
        className={classNames.join(' ')}
      >
        {currenttaxon.name}
      </button>
    );
    if (currenttaxon.children?.length) {
      nextRow.push(
        ...currenttaxon.children.map(child => ({
          ...child,
          parent: currenttaxon
        }))
      );
    }
  }

  return (
    <>
      <div className="taxon-row-outer">
        <div className="taxon-row-inner">{currentRow}</div>
      </div>
      {!!nextRow.length && (
        <TaxonomyRenderer
          taxonomy={nextRow}
          selection={nextSelection}
          level={level + 1}
          onSelection={props.onSelection}
        />
      )}
    </>
  );
};

export default TaxonomyRenderer;
