import React from 'react';
import Flexbox from '../Layout/Flexbox';
type CustomFormProps = {
  name: string;
  uuid?: string;
};
const NumberInput = (props: CustomFormProps) => {
  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
  };
  return (
    <form onSubmit={handleSubmit}>
      <Flexbox gap="1rem">
        <Flexbox flexDirection="column">
          <label htmlFor="min">min</label>
          <input id="min" type="number" name="min" value={0} />
        </Flexbox>
        <Flexbox flexDirection="column">
          <label htmlFor="max">max</label>
          <input id="max" type="number" name="max" value={0} />
        </Flexbox>
        <Flexbox flexDirection="column">
          <label htmlFor="step">step</label>
          <input id="step" type="number" name="step" value={0} />
        </Flexbox>

        <Flexbox flexDirection="column">
          <label htmlFor="unit">unit</label>
          <input id="unit" type="text" name="unit" value={''} />
        </Flexbox>
      </Flexbox>
    </form>
  );
};

export default NumberInput;
