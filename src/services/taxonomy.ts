const Description: TextInput = {
  type: 'TextInput',
  label: 'Intensity',
  value: ''
};
const OneToFive: SingleSelect = {
  type: 'SingleSelect',
  label: 'Intensity',
  custom: false,
  value: '',
  options: ['1 (Mild)', '2', '3', '4', '5 (Unbearable)']
};

const PainOneToFive: SingleSelect = { ...OneToFive, label: 'Pain' };

const Amount: NumberInput = {
  type: 'NumberInput',
  label: 'Amount',
  value: 0,
  min: 0,
  step: 1
};

const AmountMg: NumberInput = {
  ...Amount,
  label: 'Amount (mg)'
};

const Paracetamol: NumberInput = {
  ...AmountMg,
  value: 500,
  step: 50
};

const Price: NumberInput = {
  ...Amount,
  label: 'Price (€)',
  step: 0.01
};

const taxonomy: Taxonomy = [
  {
    name: 'general',
    values: [Description, Amount]
  },
  {
    name: 'consumption',
    children: [
      { name: 'drink', children: [] },
      {
        name: 'medicine',
        children: [
          { name: 'paracetamol', values: [Paracetamol] },
          { name: 'diazepam', values: [{ ...AmountMg, value: 2.5, step: 0.5 }] }
        ]
      }
    ]
  },
  {
    name: 'body',
    children: [
      { name: 'gum inflammation' },
      { name: 'stool', values: [PainOneToFive] },
      {
        name: 'pain',
        values: [Description, PainOneToFive],
        children: [
          { name: 'headache', values: [PainOneToFive] },
          { name: 'back ache', values: [PainOneToFive] },
          { name: 'tooth ache', values: [PainOneToFive] },
          { name: 'elbow ache', values: [PainOneToFive] }
        ]
      }
    ]
  },
  {
    name: 'purchase',
    children: [{ name: 'groceries', values: [Price] }]
  },
  {
    name: 'mental',
    children: [
      { name: 'dizziness', values: [OneToFive] },
      { name: 'anxiety', values: [OneToFive] },
      { name: 'dysphoria', values: [OneToFive] }
    ]
  }
];

export { taxonomy };
