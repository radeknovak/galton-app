export const cx = (...args: any[]) =>
  args.filter(arg => typeof arg === 'string').join(' ');
